[Mesh] 
  type = FileMesh
  file = molasse_LAB.msh
[]
 
[Variables]
  [./pressure]
  [../]
[]

[Kernels]
  active = 'flow euler'

  [./flow]
    type = DwarfElephantFEDarcy
    variable = pressure
    gravity_term = true
    gravity = '0  0 -9.81' ## m / s^2
    #viscosity = 5e-4 ## Pa * s
    #density = 1000 ## kg/m3
  [../]

  [./euler]
    type = DwarfElephantFEConstantSpecificStorage
    variable = pressure
  [../] 
[]

[BCs]

  [./left]
    type = DirichletBC
    variable = pressure
    boundary = 16
    value = 5e+6 # 5 MPa
  [../]
  [./right]
    type = DirichletBC
    variable = pressure
    boundary = 15
    value = 2.45e+6
  [../]
#  [./top]
#    type = DirichletBC
#    variable = pressure
#    boundary = 12
#    value = 0.0
#  [../]
  [./front]
    type = DirichletBC
    variable = pressure
    boundary = 13
    value = 3.63e+6
  [../]

[]

[Materials]

  [./Lithospheric_mantle] 
    type = DwarfElephantDFMat
    block = '1'
    Ss = 4.65e-19 # specific storage
    k = 5e-28  # permeability
  [../]
  [./Lower_Crust] 
    type = DwarfElephantDFMat
    block = '2'
    Ss = 4.65e-19 # specific storage
    k = 5e-28 # permeability
  [../]
  [./Upper_Crust] 
    type = DwarfElephantDFMat
    block = '3'
    Ss = 4.65e-19  
    k = 5e-28
  [../]
  [./Tauern_Body] 
    type = DwarfElephantDFMat
    block = '5'
    Ss = 4.65e-19  
    k = 5e-28
  [../]
  [./Alpine_Body] 
    type = DwarfElephantDFMat
    block = '4'
    Ss = 4.65e-19 
    k = 5e-28 
  [../]
  [./Folded_Molasse] 
    type = DwarfElephantDFMat
    block = '6'
    Ss = 1.35e-11
    k = 1e-14
  [../]
  [./Foreland_Molasse]  
    type = DwarfElephantDFMat
    block = '8'
    Ss = 1.35e-11 
    k =  1e-13
  [../]
  [./Cretaceous] 
    type = DwarfElephantDFMat
    block = '7'
    Ss = 1.35e-11
    k = 2.5e-11 
  [../]
  [./Upper_Jurassic] # (Malm)
    type = DwarfElephantDFMat
    block = '9'
    Ss = 2.79e-11  
    k = 5e-16
  [../]
  [./PreMalm] 
    type = DwarfElephantDFMat
    block = '10'
    Ss = 2.37e-11
    k = 2.5e-12
  [../]
[]

[Preconditioning]
  active = 'SMP'
  [./SMP]
    type = SMP
    full = true
    solve_type = 'NEWTON'
    petsc_options_iname = '-pc_type -sub_pc_type -snes_linesearch_type -ksp_gmres_restart -pc_gamg_sym_graph'
    petsc_options_value = 'gasm hypre cp 301 true'
  [../]
[]

[Executioner]
  type = Transient
  start_time = 0
  num_steps = 50
  l_max_its = 60
  nl_max_its = 30
  dt = 1e+6

  nl_abs_tol = 1e-7

  [./TimeIntegrator]
    type = CrankNicolson
  [../]
[]


[Outputs]
  perf_graph = true
  execute_on = 'timestep_end'
  csv = false
  [./out]
    type = Exodus
  [../]
[]
