[Mesh]
  type = GeneratedMesh
  #file = b1.msh
  dim = 3
  nx = 50
  ny = 50
  nz = 50
  xmin = 0
  xmax = 10
  ymin = 0
  ymax = 10
  zmin = 0
  zmax = 10
[]

[GlobalParams]
  pore_pressure = pore_pressure
  temperature = temperature
[]

[Variables]
  [./pore_pressure]
    order = FIRST
    family = LAGRANGE
    initial_condition = 0.0
  [../]
  [./temperature]
    order = FIRST
    family = LAGRANGE
   initial_condition = 0.0
  [../]
[]

[Kernels]
  [./HKernel]
    type = GolemKernelH
    variable = pore_pressure
  [../]
  [./temp_time]
    type = GolemKernelTimeT
    variable = temperature
  [../]
  [./THKernel]
    type = GolemKernelTH
    variable = temperature
    is_conservative = true
  [../]
[]

[AuxVariables]
  [./vx]
    order = CONSTANT
    family = MONOMIAL
  [../]
  [./vy]
    order = CONSTANT
    family = MONOMIAL
  [../]
  [./vz]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[AuxKernels]
  [./darcyx]
    type = GolemDarcyVelocity
    variable = vx
    component = 0
  [../]
  [./darcyy]
    type = GolemDarcyVelocity
    variable = vy
    component = 1
  [../]
  [./darcyz]
    type = GolemDarcyVelocity
    variable = vz
    component = 2
  [../]
[]

[Functions]
  [./T0_func]
    type = ConstantFunction
    value = 50.0
  [../]
[]

[BCs]
  [./p_left]
    type = DirichletBC
    variable = pore_pressure
    boundary = 4
    value = 4e+5
  [../]
  [./p_right]
    type = DirichletBC
    variable = pore_pressure
    boundary = 2
    value = 1e+5
  [../]
  [./T_top]
    type = DirichletBC
    variable = temperature
    boundary = 3
    value = 10
  [../]
  [./T_bot]
    type = FunctionDirichletBC
    variable = temperature
    boundary = 1
    function = T0_func
    [../]
[]

[Materials]
  [./TH1]
    type = GolemMaterialTH
    #block = '7 9'
    porosity_initial = 0.3
    permeability_initial = 1.0e-9
    fluid_viscosity_initial = 5.0e-4
    fluid_density_initial = 1000
    solid_density_initial = 2000
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 1.25
    fluid_heat_capacity_initial = 1.1
    solid_heat_capacity_initial = 0.5
    porosity_uo = porosity
    fluid_density_uo = fluid_density
    fluid_viscosity_uo = fluid_viscosity
    permeability_uo = permeability
  [../]
 # [./TH2]
 #   type = GolemMaterialTH
 #   block = 8
 #   porosity_initial = 0.2
 #   permeability_initial = 1.0e-11
 #   fluid_viscosity_initial = 5.0e-4
 #   fluid_density_initial = 1000
 #   solid_density_initial = 3000
 #   fluid_thermal_conductivity_initial = 0.65
 #   solid_thermal_conductivity_initial = 2.5
 #   fluid_heat_capacity_initial = 1.1
 #   solid_heat_capacity_initial = 0.65
 #   porosity_uo = porosity
 #   fluid_density_uo = fluid_density
 #   fluid_viscosity_uo = fluid_viscosity
 #   permeability_uo = permeability
 # [../]
[]

[UserObjects]
  [./porosity]
    type = GolemPorosityConstant
  [../]
  [./fluid_density]
    type = GolemFluidDensityConstant
  [../]
  [./fluid_viscosity]
    type = GolemFluidViscosityConstant
  [../]
  [./permeability]
    type = GolemPermeabilityConstant
  [../]
[]

[Preconditioning]
  [./fieldsplit]
    type = FSP
    topsplit = pT
    [./pT]
      splitting = 'p T'
      splitting_type = multiplicative
      petsc_options_iname = '-snes_type -snes_linesearch_type
                             -snes_atol -snes_rtol -snes_max_it'
      petsc_options_value = 'newtonls basic
                             1.0e-05 1.0e-12 25'
    [../]
    [./p]
     vars = 'pore_pressure'
     petsc_options_iname = '-pc_type -pc_hypre_type'
     petsc_options_value = 'hypre boomeramg'
    [../]
    [./T]
     vars = 'temperature'
     petsc_options_iname = '-pc_type -pc_hypre_type'
     petsc_options_value = 'hypre boomeramg'
    [../]
  [../]
[]

[Executioner]
  type = Transient
  start_time = 0
  num_steps = 50
  l_max_its = 20
  nl_max_its = 10
  dtmin = 2e-10

  nl_abs_tol = 1e-7

  [./TimeIntegrator]
    type = CrankNicolson
  [../]

  [./TimeStepper]
    growth_factor = 2.2
    dt = 1.0e-8
    cutback_factor = 0.8
    type = IterationAdaptiveDT
    optimal_iterations = 10
  [../]
[]

[Outputs]
  print_linear_residuals = true
  perf_graph = true
  exodus = true
[]
