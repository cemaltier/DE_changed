[Mesh] 
  type = FileMesh
  file = molasse_LAB.msh
[]
 
[Variables]
  [./pressure]
    order = FIRST
    family = LAGRANGE
  [../]
  [./temp]
    order = FIRST
    family = LAGRANGE
  [../]
[]
 
[AuxVariables]
  [./velocity_x]
    order = CONSTANT
    family = MONOMIAL
  [../]

  [./velocity_y]
    order = CONSTANT 
    family = MONOMIAL
  [../]

  [./velocity_z]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[Kernels]
  active = 'HKernel THKernel temp_time'

  [./HKernel]
    type = GolemKernelH
    variable = pressure
  [../]
  [./temp_time]
    type = GolemKernelTimeT
    variable = temp
  [../]
  [./THKernel]
    type = GolemKernelTH
    variable = temp
    pore_pressure = pressure
    is_conservative = true
  [../]
[]

[AuxKernels]
  [./velocity_x_aux]
    type = GolemDarcyVelocity
    variable = velocity_x
    pore_pressure = pressure
    component = 0
  [../]

  [./velocity_y_aux]
    type = GolemDarcyVelocity
    variable = velocity_y
    pore_pressure = pressure
    component = 1 
  [../]

  [./velocity_z_aux]
    type = GolemDarcyVelocity
    variable = velocity_z
    pore_pressure = pressure
    component = 2
  [../]
[]


[BCs]
  [./top_temp]
    type = DirichletBC
    variable = temp
    boundary = 12
    value = 10 # Annual average surface temp.
  [../]
  [./bottom_temp]
    type = DirichletBC
    variable = temp
    boundary = 11
    value = 1300 # LAB temperature (1300-1500 C)
  [../]
  [./left]
    type = DirichletBC
    variable = pressure
    boundary = 16
    value = 5e+6 # 5 MPa
  [../]
  [./right]
    type = DirichletBC
    variable = pressure
    boundary = 15
    value = 2.45e+6
  [../]
  [./top]
    type = DirichletBC
    variable = pressure
    boundary = 12
    value = 0.0
  [../]
  [./front]
    type = DirichletBC
    variable = pressure
    boundary = 13
    value = 3.63e+6
  [../]
[]

[Materials]

  [./Lithospheric_mantle] 
    type = GolemMaterialTH
    block = '1'
 #  has_heat_source_sink = true
 #   heat_source_sink = 3e-8 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 3.0
    solid_heat_capacity_initial = 0.350
    fluid_heat_capacity_initial = 4.200
    solid_density_initial = 3.300
    fluid_density_initial = 1.000
    fluid_viscosity_initial = 5e-4
    porosity_initial = 1.0e-9
    permeability_initial = 5e-28
    porosity_uo = poro
    fluid_density_uo = f_de
    fluid_viscosity_uo = f_vis
    permeability_uo = perm
    pore_pressure = pressure
    temperature = temp
  [../]
  [./Lower_Crust] 
    type = GolemMaterialTH
    block = '2'
#    has_heat_source_sink = true
#    heat_source_sink = 7e-7
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.7
    solid_heat_capacity_initial = 0.320
    fluid_heat_capacity_initial = 4.200
    solid_density_initial = 3.100
    fluid_density_initial = 1.000
    fluid_viscosity_initial = 5e-4
    porosity_initial = 1.0e-9
    permeability_initial = 5e-28
    porosity_uo = poro
    fluid_density_uo = f_de
    fluid_viscosity_uo = f_vis
    permeability_uo = perm
    pore_pressure = pressure
    temperature = temp
  [../]
  [./Upper_Crust] 
    type = GolemMaterialTH
    block = '3'
#    has_heat_source_sink = true
#    heat_source_sink = 1.8e-6 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 3.1
    solid_heat_capacity_initial = 0.320
    fluid_heat_capacity_initial = 4.200
    solid_density_initial = 2.700
    fluid_density_initial = 1.000
    fluid_viscosity_initial = 5e-4
    porosity_initial = 1.0e-9
    permeability_initial = 5e-28
    porosity_uo = poro
    fluid_density_uo = f_de
    fluid_viscosity_uo = f_vis
    permeability_uo = perm
    pore_pressure = pressure
    temperature = temp
  [../]
  [./Tauern_Body] 
    type = GolemMaterialTH
    block = '5'
#    has_heat_source_sink = true
#    heat_source_sink = 1.8e-6 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.6
    solid_heat_capacity_initial = 0.320
    fluid_heat_capacity_initial = 4.200
    solid_density_initial = 2.780
    fluid_density_initial = 1.000
    fluid_viscosity_initial = 5e-4
    porosity_initial = 1.0e-9
    permeability_initial = 5e-28
    porosity_uo = poro
    fluid_density_uo = f_de
    fluid_viscosity_uo = f_vis
    permeability_uo = perm
    pore_pressure = pressure
    temperature = temp
  [../]
  [./Alpine_Body] 
    type = GolemMaterialTH
    block = '4'
#    has_heat_source_sink = true
#    heat_source_sink = 3e-7 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.2
    solid_heat_capacity_initial = 0.320
    fluid_heat_capacity_initial = 4.200
    solid_density_initial = 2.700
    fluid_density_initial = 1.000
    fluid_viscosity_initial = 5e-4
    porosity_initial = 1.0e-9
    permeability_initial = 5e-28
    porosity_uo = poro
    fluid_density_uo = f_de
    fluid_viscosity_uo = f_vis
    permeability_uo = perm
    pore_pressure = pressure
    temperature = temp
  [../]
  [./Folded_Molasse] 
    type = GolemMaterialTH
    block = '6'
#    has_heat_source_sink = true
#    heat_source_sink = 1e-6 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.1
    solid_heat_capacity_initial = 0.250
    fluid_heat_capacity_initial = 4.200
    solid_density_initial = 2.400
    fluid_density_initial = 1.000
    fluid_viscosity_initial = 5e-4
    porosity_initial = 0.029
    permeability_initial = 1e-14
    porosity_uo = poro
    fluid_density_uo = f_de
    fluid_viscosity_uo = f_vis
    permeability_uo = perm
    pore_pressure = pressure
    temperature = temp
  [../]
  [./Foreland_Molasse]  
    type = GolemMaterialTH
    block = '8'
#    has_heat_source_sink = true
#    heat_source_sink = 1e-6 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.1
    solid_heat_capacity_initial = 0.250
    fluid_heat_capacity_initial = 4.200
    solid_density_initial = 2.350
    fluid_density_initial = 1.000
    fluid_viscosity_initial = 5e-4
    porosity_initial = 0.029
    permeability_initial = 1e-13
    porosity_uo = poro
    fluid_density_uo = f_de
    fluid_viscosity_uo = f_vis
    permeability_uo = perm
    pore_pressure = pressure
    temperature = temp
  [../]
  [./Cretaceous] 
    type = GolemMaterialTH
    block = '7'
#    has_heat_source_sink = true
#    heat_source_sink = 1.4e-6
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.4
    solid_heat_capacity_initial = 0.250
    fluid_heat_capacity_initial = 4.200
    solid_density_initial = 2.640
    fluid_density_initial = 1.000
    fluid_viscosity_initial = 5e-4
    porosity_initial = 0.029
    permeability_initial = 2.5e-11 
    porosity_uo = poro
    fluid_density_uo = f_de
    fluid_viscosity_uo = f_vis
    permeability_uo = perm
    pore_pressure = pressure
    temperature = temp
  [../]
  [./Upper_Jurassic] # Malm
    type = GolemMaterialTH
    block = '9'
#    has_heat_source_sink = true
#    heat_source_sink = 1.4e-6 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.6
    solid_heat_capacity_initial = 0.270
    fluid_heat_capacity_initial = 4.200
    solid_density_initial = 2.650
    fluid_density_initial = 1.000
    fluid_viscosity_initial = 5e-4
    porosity_initial = 0.06
    permeability_initial = 5e-16 
    porosity_uo = poro
    fluid_density_uo = f_de
    fluid_viscosity_uo = f_vis
    permeability_uo = perm
    pore_pressure = pressure
    temperature = temp
  [../]
  [./PreMalm] 
    type = GolemMaterialTH
    block = '10'
#    has_heat_source_sink = true
#    heat_source_sink = 1e-6 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.7
    solid_heat_capacity_initial = 0.230 
    fluid_heat_capacity_initial = 4.200
    solid_density_initial = 2.680
    fluid_density_initial = 1.000
    fluid_viscosity_initial = 5e-4
    porosity_initial = 0.051
    permeability_initial = 2.5e-12
    porosity_uo = poro
    fluid_density_uo = f_de
    fluid_viscosity_uo = f_vis
    permeability_uo = perm
    pore_pressure = pressure
    temperature = temp
  [../]
[]

[UserObjects]
  [./poro]
    type = GolemPorosityConstant
  [../]
  [./f_de]
    type = GolemFluidDensityConstant
  [../]
  [./f_vis]
    type = GolemFluidViscosityConstant
  [../]
  [./perm]
    type = GolemPermeabilityConstant
  [../]
[]

[Preconditioning]
  active = 'SMP'
  [./SMP]
    type = SMP
    full = true
    solve_type = 'NEWTON'
    petsc_options_iname = '-pc_type -sub_pc_type -snes_linesearch_type -ksp_gmres_restart -pc_gamg_sym_graph'
    petsc_options_value = 'gasm hypre cp 301 true'
  [../]

  [./FSP]
    type = FSP
    full = true
    solve_type = 'NEWTON'
    topsplit = 'pt'
    [./pt]
      splitting = 'pressure temp'
    [../]
    [./pressure]
      vars = 'pressure'
      petsc_options_iname = '-pc_type -sub_pc_type -snes_linesearch_type -ksp_gmres_restart'
      petsc_options_value = 'gamg hypre cp 151'
    [../]
    [./temp]
      vars = 'temp'
      petsc_options_iname = '-pc_type -sub_pc_type -snes_linesearch_type -ksp_gmres_restart'
      petsc_options_value = 'gasm hypre cp 151'
    [../]
  [../]
[]

[Executioner]

   type = Transient
   start_time = 0
   num_steps = 25
   l_max_its = 60
   nl_max_its = 30

   nl_abs_tol = 1e-7

  [./TimeIntegrator]
    type = CrankNicolson
  [../]  
  
  [./TimeStepper]

    growth_factor = 2.2
    dt = 2.5e+5
    cutback_factor = 0.8
    type = IterationAdaptiveDT
    optimal_iterations = 10
  [../]
[]

[Outputs]
  perf_graph = true
  execute_on = 'timestep_end'
  csv = false
  [./out]
    type = Exodus
  [../]
[]
