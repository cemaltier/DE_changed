[Mesh]
  type = FileMesh
  file = molasse_LAB.msh
[]

[Variables]
  [./temp]
  [../]
[]

[Kernels]

  [./conduction]
    type = DwarfElephantFEThermalConduction
    variable = temp
    transient = true
  [../]
  [./radiogenic_heat]
    type = DwarfElephantFEConstantRadiogenicHeatProduction
    variable = temp
    transient = true
  [../]
  [./euler]
    type = TimeDerivative 
    variable = temp
  [../]
[]

[Preconditioning]
 active = 'SMP'

  [./SMP]
    type = SMP
    full = true
    solve_type = 'NEWTON'
    petsc_options_iname = '-pc_type -sub_pc_type -snes_linesearch_type -ksp_gmres_restart -pc_gamg_sym_graph'
    petsc_options_value = 'gasm hypre cp 301 true'
  [../]
[]

[BCs]

  [./top_temp]
    type = DirichletBC
    variable = temp
    boundary = 12
    value = 0 # Annual average surface temp.
  [../]
  [./bottom_temp]
    type = DirichletBC
    variable = temp
    boundary = 11
    value = 1300 # LAB temperature (1300-1500 C)
  [../]
[]

[Materials]

  [./Lithospheric_mantle] 
    type = DwarfElephantTCMat
    block = '1'
    TC = 3.0    # thermal conductivity
    HP = 3e-8   # radiogenic heat production
    D = 3305    # density
    SH = 3.5    # specific heat
  [../]
  [./Lower_Crust] 
    type = DwarfElephantTCMat
    block = '2'
    TC = 2.7  
    HP = 7e-7
    D = 3100
    SH = 3.2
  [../]
  [./Upper_Crust] 
    type = DwarfElephantTCMat
    block = '3'
    TC = 3.1 
    HP = 1.8e-6 
    D = 2700
    SH = 3.2
  [../]
  [./Tauern_Body] 
    type = DwarfElephantTCMat
    block = '5'
    TC = 2.6 
    HP = 1.8e-6 
    D = 2800
    SH = 3.2
  [../]
  [./Alpine_Body] 
    type = DwarfElephantTCMat
    block = '4'
    TC = 2.2  
    HP = 3e-7 
    D = 2700
    SH = 3.2
  [../]
  [./Folded_Molasse] 
    type = DwarfElephantTCMat
    block = '6'
    TC = 2.1
    HP = 1e-6 
    D = 2400
    SH = 2.5
  [../]
  [./Foreland_Molasse]  
    type = DwarfElephantTCMat
    block = '8'
    TC = 2.1 
    HP = 1e-6 
    D = 2350
    SH = 2.5
  [../]
  [./Cretaceous] 
    type = DwarfElephantTCMat
    block = '7'
    TC = 2.4
    HP = 1.4e-6
    D = 2640
    SH = 2.5
  [../]
  [./Upper_Jurassic] # Malm
    type = DwarfElephantTCMat
    block = '9'
    TC = 2.7
    HP = 1.4e-6 
    D = 2650
    SH = 2.3
  [../]
  [./PreMalm] 
    type = DwarfElephantTCMat
    block = '10'
    TC = 2.7
    HP = 1e-6 
    D = 2680
    SH = 2.3
  [../]
[]

[Executioner]
  type = Transient
  start_time = 0
  num_steps = 50
  l_max_its = 60
  nl_max_its = 30
  dt = 1e+12

  nl_abs_tol = 1e-7

  [./TimeIntegrator]
    type = CrankNicolson
  [../]

#  [./TimeStepper]
#    growth_factor = 1.2
#    dt = 2.5e-5
#    cutback_factor = 0.8
#    type = IterationAdaptiveDT
#    optimal_iterations = 20
#  [../]
[]


[Outputs]
  perf_graph = true
  execute_on = 'timestep_end'
  csv = false
  [./out]
    type = Exodus
  [../]
[]
