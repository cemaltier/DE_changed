[Mesh]
  type = FileMesh
  file = molasse_LAB.msh
[]

[Variables]
  [./temp]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[Kernels]
  active = 'conduction euler'

  [./conduction]
    type = GolemKernelT
    variable = temp
  [../]
  [./euler]
    type = GolemKernelTimeT 
    variable = temp
  [../]
[]

[Preconditioning]
 active = 'SMP'

  [./SMP]
    type = SMP
    full = true
    solve_type = 'NEWTON'
    petsc_options_iname = '-pc_type -sub_pc_type -snes_linesearch_type -ksp_gmres_restart -pc_gamg_sym_graph'
    petsc_options_value = 'gasm hypre cp 301 true'
  [../]
[]

[BCs]

  [./top_temp]
    type = DirichletBC
    variable = temp
    boundary = 12
    value = 10 # Annual average surface temp.
  [../]
  [./bottom_temp]
    type = DirichletBC
    variable = temp
    boundary = 11
    value = 1300 # LAB temperature (1300-1500 C)
  [../]
[]

[Materials]

  [./Lithospheric_mantle] 
    type = GolemMaterialT
    block = '1'
    has_heat_source_sink = true
    heat_source_sink = 3e-8 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 3.0
    solid_heat_capacity_initial = 350
    solid_density_initial = 3300
    porosity_uo = porosity
    fluid_density_uo = fluid_density
  [../]
  [./Lower_Crust] 
    type = GolemMaterialT
    block = '2'
    has_heat_source_sink = true
    heat_source_sink =  7e-7
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.7
    solid_heat_capacity_initial = 320
    solid_density_initial = 3100
    porosity_uo = porosity
    fluid_density_uo = fluid_density
  [../]
  [./Upper_Crust] 
    type = GolemMaterialT
    block = '3'
    has_heat_source_sink = true
    heat_source_sink = 1.8e-6 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 3.1
    solid_heat_capacity_initial = 320
    solid_density_initial = 2700
    porosity_uo = porosity
    fluid_density_uo = fluid_density
  [../]
  [./Tauern_Body] 
    type = GolemMaterialT
    block = '5'
    has_heat_source_sink = true
    heat_source_sink = 1.8e-6 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.6
    solid_heat_capacity_initial = 320
    solid_density_initial = 2780
    porosity_uo = porosity
    fluid_density_uo = fluid_density
  [../]
  [./Alpine_Body] 
    type = GolemMaterialT
    block = '4'
    has_heat_source_sink = true
    heat_source_sink = 3e-7 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.2
    solid_heat_capacity_initial = 320
    solid_density_initial = 2700
    porosity_uo = porosity
    fluid_density_uo = fluid_density
  [../]
  [./Folded_Molasse] 
    type = GolemMaterialT
    block = '6'
    has_heat_source_sink = true
    heat_source_sink = 1e-6 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.1
    solid_heat_capacity_initial = 250
    solid_density_initial = 2400
    porosity_uo = porosity
    fluid_density_uo = fluid_density
  [../]
  [./Foreland_Molasse]  
    type = GolemMaterialT
    block = '8'
    has_heat_source_sink = true
    heat_source_sink = 1e-6 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.1
    solid_heat_capacity_initial = 250
    solid_density_initial = 2350
    porosity_uo = porosity
    fluid_density_uo = fluid_density
  [../]
  [./Cretaceous] 
    type = GolemMaterialT
    block = '7'
    has_heat_source_sink = true
    heat_source_sink = 1.4e-6
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.4
    solid_heat_capacity_initial = 250
    solid_density_initial = 2640
    porosity_uo = porosity
    fluid_density_uo = fluid_density
  [../]
  [./Upper_Jurassic] # Malm
    type = GolemMaterialT
    block = '9'
    has_heat_source_sink = true
    heat_source_sink = 1.4e-6 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.6
    solid_heat_capacity_initial = 270
    solid_density_initial = 2650
    porosity_uo = porosity
    fluid_density_uo = fluid_density
  [../]
  [./PreMalm] 
    type = GolemMaterialT
    block = '10'
    has_heat_source_sink = true
    heat_source_sink = 1e-6 
    fluid_thermal_conductivity_initial = 0.65
    solid_thermal_conductivity_initial = 2.7
    solid_heat_capacity_initial = 230 
    solid_density_initial = 2680
    porosity_uo = porosity
    fluid_density_uo = fluid_density
  [../]
[]

[UserObjects]
  [./porosity]
    type = GolemPorosityConstant
  [../]
  [./fluid_density]
    type = GolemFluidDensityConstant
  [../]
[]

[Postprocessors]
  [temp_end]
    type = ElementAverageValue
    variable = temp
    execute_on = 'INITIAL TIMESTEP_END'
  [../]
[]


[Executioner]
#  type = Steady
#  solve_type = 'NEWTON'

  type = Transient
  start_time = 0
  num_steps = 50
  l_max_its = 60
  nl_max_its = 30
  dt = 1e+14

  nl_abs_tol = 1e-7

  [./TimeIntegrator]
    type = CrankNicolson
  [../]

#  [./TimeStepper]
#    growth_factor = 1.2
#    dt = 2.5e-5
#    cutback_factor = 0.8
#    type = IterationAdaptiveDT
#    optimal_iterations = 20
#  [../]
[]


[Outputs]
  perf_graph = true
  execute_on = 'timestep_end'
  csv = false
  [./out]
    type = Exodus
  [../]
[]
