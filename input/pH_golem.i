[Mesh] 
  type = FileMesh
  file = molasse_LAB.msh
[]
 
[Variables]
  [./pressure]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[Kernels]
  active = 'flow euler'

  [./flow]
    type = GolemKernelH
    variable = pressure
  [../]

  [./euler]
    type = GolemKernelTimeH
    variable = pressure
  [../] 
[]

[BCs]

  [./left]
    type = DirichletBC
    variable = pressure
    boundary = 16
    value = 5e+6 # 5 MPa
  [../]
  [./right]
    type = DirichletBC
    variable = pressure
    boundary = 15
    value = 2.45e+6
  [../]
#  [./top]
#    type = DirichletBC
#    variable = pressure
#    boundary = 12
#    value = 0.0
#  [../]
  [./front]
    type = DirichletBC
    variable = pressure
    boundary = 13
    value = 3.63e+6
  [../]

[]

[Materials]

  [./Lithospheric_mantle] 
    type = GolemMaterialH
    block = '1'
    permeability_initial = 5e-28
    fluid_viscosity_initial = 5e-4
    porosity_initial = 1.0e-9
    fluid_modulus = 5.0e+09
    porosity_uo = porosity
    fluid_density_uo = fluid_density
    fluid_viscosity_uo = fluid_viscosity
    permeability_uo = permeability
  [../]
  [./Lower_Crust] 
    type = GolemMaterialH
    block = '2'
    permeability_initial = 5e-28
    fluid_viscosity_initial = 5e-4
    porosity_initial = 1.0e-9
    fluid_modulus = 5.0e+09
    porosity_uo = porosity
    fluid_density_uo = fluid_density
    fluid_viscosity_uo = fluid_viscosity
    permeability_uo = permeability
  [../]
  [./Upper_Crust] 
    type = GolemMaterialH
    block = '3'
    permeability_initial = 5e-28
    fluid_viscosity_initial = 5e-4
    porosity_initial = 1.0e-9
    fluid_modulus = 5.0e+09
    porosity_uo = porosity
    fluid_density_uo = fluid_density
    fluid_viscosity_uo = fluid_viscosity
    permeability_uo = permeability
  [../]
  [./Tauern_Body] 
    type = GolemMaterialH
    block = '5'
    permeability_initial = 5e-28
    fluid_viscosity_initial = 5e-4
    porosity_initial = 1.0e-9
    fluid_modulus = 5.0e+09
    porosity_uo = porosity
    fluid_density_uo = fluid_density
    fluid_viscosity_uo = fluid_viscosity
    permeability_uo = permeability
  [../]
  [./Alpine_Body] 
    type = GolemMaterialH
    block = '4'
    permeability_initial = 5e-28
    fluid_viscosity_initial = 5e-4
    porosity_initial = 1.0e-9
    fluid_modulus = 5.0e+09
    porosity_uo = porosity
    fluid_density_uo = fluid_density
    fluid_viscosity_uo = fluid_viscosity
    permeability_uo = permeability
  [../]
  [./Folded_Molasse] 
    type = GolemMaterialH
    block = '6'
    permeability_initial = 1e-14
    fluid_viscosity_initial = 5e-4
    porosity_initial = 0.029
    fluid_modulus = 5.0e+09
    porosity_uo = porosity
    fluid_density_uo = fluid_density
    fluid_viscosity_uo = fluid_viscosity
    permeability_uo = permeability
  [../]
  [./Foreland_Molasse]  
    type = GolemMaterialH
    block = '8'
    permeability_initial =  1e-13 
    fluid_viscosity_initial = 5e-4
    porosity_initial = 0.029
    fluid_modulus = 5.0e+09
    porosity_uo = porosity
    fluid_density_uo = fluid_density
    fluid_viscosity_uo = fluid_viscosity
    permeability_uo = permeability
  [../]
  [./Cretaceous] 
    type = GolemMaterialH
    block = '7'
    permeability_initial = 2.5e-11 
    fluid_viscosity_initial = 5e-4
    porosity_initial = 0.029
    fluid_modulus = 5.0e+09
    porosity_uo = porosity
    fluid_density_uo = fluid_density
    fluid_viscosity_uo = fluid_viscosity
    permeability_uo = permeability 
  [../]
  [./Upper_Jurassic] # (Malm)
    type = GolemMaterialH
    block = '9'
    permeability_initial = 5e-16
    fluid_viscosity_initial = 5e-4
    porosity_initial = 0.06
    fluid_modulus = 5.0e+09
    porosity_uo = porosity
    fluid_density_uo = fluid_density
    fluid_viscosity_uo = fluid_viscosity
    permeability_uo = permeability
  [../]
  [./PreMalm] 
    type = GolemMaterialH
    block = '10'
    permeability_initial =  2.5e-12
    fluid_viscosity_initial = 5e-4
    porosity_initial = 0.051
    fluid_modulus = 5.0e+09
    porosity_uo = porosity
    fluid_density_uo = fluid_density
    fluid_viscosity_uo = fluid_viscosity
    permeability_uo = permeability
  [../]
[]

[Preconditioning]
  active = 'SMP'
  [./SMP]
    type = SMP
    full = true
    solve_type = 'NEWTON'
    petsc_options_iname = '-pc_type -sub_pc_type -snes_linesearch_type -ksp_gmres_restart -pc_gamg_sym_graph'
    petsc_options_value = 'gasm hypre cp 301 true'
  [../]
[]

[UserObjects]
  [./porosity]
    type = GolemPorosityConstant
  [../]
  [./fluid_density]
    type = GolemFluidDensityConstant
  [../]
  [./fluid_viscosity]
    type = GolemFluidViscosityConstant
  [../]
  [./permeability]
    type = GolemPermeabilityConstant
  [../]
[]

[Postprocessors]
  [pressure_end]
    type = ElementAverageValue
    variable = pressure
    execute_on = 'INITIAL TIMESTEP_END'
  [../]
[]

[Executioner]
#  type = Steady
#  solve_type = 'NEWTON'

  type = Transient
  start_time = 0
  num_steps = 50
  l_max_its = 60
  nl_max_its = 30
  dt = 1e+6

  nl_abs_tol = 1e-7

  [./TimeIntegrator]
    type = CrankNicolson
  [../]
[]


[Outputs]
  perf_graph = true
  execute_on = 'timestep_end'
  csv = false
  [./out]
    type = Exodus
  [../]
[]
