#ifndef DWARFELEPHANTDFMAT_H
#define DWARFELEPHANTDFMAT_H

#include "Material.h"
#include "Function.h"

//Forward Declarations
class DwarfElephantDFMat;
class Function;

template<>
InputParameters validParams<DwarfElephantDFMat>();

class DwarfElephantDFMat : public Material
{
public:
  DwarfElephantDFMat(const InputParameters & parameters);

protected:
  virtual void computeQpProperties() override;

  MaterialProperty<Real> & _Ss;
  const Function & _func1;
  MaterialProperty<Real> & _k;
  const Function & _func2;
};

#endif //DwarfElephantDFMat_H
