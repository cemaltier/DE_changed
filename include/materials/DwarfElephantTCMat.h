#ifndef DWARFELEPHANTTCMAT_H
#define DWARFELEPHANTTCMAT_H

#include "Material.h"
#include "Function.h"

//Forward Declarations
class DwarfElephantTCMat;
class Function;

template<>
InputParameters validParams<DwarfElephantTCMat>();

class DwarfElephantTCMat : public Material
{
public:
  DwarfElephantTCMat(const InputParameters & parameters);

protected:
  virtual void computeQpProperties() override;

  MaterialProperty<Real> & _TC;
  const Function & _func1;
  MaterialProperty<Real> & _HP;
  const Function & _func2;
  MaterialProperty<Real> & _SH;
  const Function & _func3;
  MaterialProperty<Real> & _D;
  const Function & _func4;

};

#endif //DwarfElephantTCMat_H
